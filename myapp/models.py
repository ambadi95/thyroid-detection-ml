from django.db import models

# Create your models here.
class user_login(models.Model):
    uname = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    utype = models.CharField(max_length=50)

    def __str__(self):
        return self.uname


class patient_details(models.Model):
    user_id = models.IntegerField()
    fname = models.CharField(max_length=50)
    lname = models.CharField(max_length=50)
    pno = models.CharField(max_length=50)
    dob = models.CharField(max_length=50)
    gender = models.CharField(max_length=50)
    addr = models.CharField(max_length=500)
    pincode = models.CharField(max_length=50)
    email = models.CharField(max_length=150)
    conatct = models.CharField(max_length=50)
    dt = models.CharField(max_length=50)
    tm = models.CharField(max_length=50)
    status = models.CharField(max_length=50)

    def __str__(self):
        return self.fname

class patient_report(models.Model):
    patient_id = models.IntegerField()
    file_path = models.CharField(max_length=500)
    result = models.CharField(max_length=150)
    dt = models.CharField(max_length=50)
    tm = models.CharField(max_length=50)
    status = models.CharField(max_length=50)

    def __str__(self):
        return self.file_path

class category_master(models.Model):
    category_name = models.CharField(max_length=150)

    def __str__(self):
        return self.category_name

class data_set_master(models.Model):
    category_id = models.IntegerField()
    pic_path = models.CharField(max_length=500)
    dt = models.CharField(max_length=50)
    tm = models.CharField(max_length=50)
    status = models.CharField(max_length=50)

    def __str__(self):
        return self.pic_path




