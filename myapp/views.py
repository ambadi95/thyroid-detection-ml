from django.shortcuts import render
from .models import user_login,category_master,data_set_master,patient_details,patient_report
from django.db.models import Max
from django.core.files.storage import FileSystemStorage
from .algo_impl import orb_compute,selfile
from datetime import datetime
import os
# Create your views here.
def index(request):
    return render(request,'./medico/index.html')

def about(request):
    return render(request,'./medico/about.html')

def contact(request):
    return render(request,'./medico/contact.html')

def admin_login(request):
    if request.method == 'POST':
        uname = request.POST.get('uname')
        passwd = request.POST.get('passwd')

        ul = user_login.objects.filter(uname=uname, password=passwd)

        if len(ul) == 1:
            request.session['user_id'] = ul[0].uname
            context = {'uname': request.session['user_id']}
            return render(request, 'medico/182 Medico -DOC/admin_home.html',
                          context)
        else:
            return render(request, 'medico/admin_login.html')
    else:
        return render(request, 'medico/admin_login.html')

def admin_home(request):

    context = {'uname':request.session['user_id']}
    return render(request,'./medico/182 Medico -DOC/admin_home.html',context)

def admin_settings(request):

    context = {'uname':request.session['user_id']}
    return render(request,'./medico/182 Medico -DOC/admin_settings.html',context)

def admin_settings_404(request):

    context = {'uname':request.session['user_id']}
    return render(request,'./medico/182 Medico -DOC/admin_settings_404.html',context)

def admin_changepassword(request):
    if request.method == 'POST':
        uname = request.session['user_id']
        new_password = request.POST.get('new_password')
        current_password = request.POST.get('current_password')
        print("username:::" + uname)
        print("current_password" + str(current_password))

        try:

            ul = user_login.objects.get(uname=uname, password=current_password)

            if ul is not None:
                ul.password = new_password  # change field
                ul.save()
                return render(request, './medico/182 Medico -DOC/admin_settings.html')
            else:
                return render(request, './medico/182 Medico -DOC/admin_settings.html')
        except user_login.DoesNotExist:
            return render(request, './medico/182 Medico -DOC/admin_changepassword.html')
    else:
        return render(request, './medico/182 Medico -DOC/admin_changepassword.html')

def admin_category_master_add(request):
    if request.method == 'POST':
        category_name = request.POST.get('category_name')
        cm = category_master(category_name=category_name)
        cm.save()
        return render(request, 'medico/182 Medico -DOC/admin_category_master_add.html')

    else:
        return render(request, 'medico/182 Medico -DOC/admin_category_master_add.html')

def admin_category_master_delete(request):
    id = request.GET.get('id')
    print("id="+id)

    nm = category_master.objects.get(id=int(id))
    nm.delete()

    nm_l = category_master.objects.all()
    context ={'category_list':nm_l}
    return render(request,'medico/182 Medico -DOC/admin_category_master_view.html',context)

def admin_category_master_view(request):
    nm_l = category_master.objects.all()
    context ={'category_list':nm_l}
    return render(request,'medico/182 Medico -DOC/admin_category_master_view.html',context)


def admin_staff_user_add(request):
    if request.method == 'POST':
        uname = request.POST.get('uname')
        password = request.POST.get('password')
        utype = 'staff'

        ul = user_login(uname=uname,password=password,utype=utype)
        ul.save()
        return render(request, 'medico/182 Medico -DOC/admin_staff_user_add.html')

    else:
        return render(request, 'medico/182 Medico -DOC/admin_staff_user_add.html')

def admin_staff_user_delete(request):
    id = request.GET.get('id')
    print("id="+id)

    nm = user_login.objects.get(id=int(id))
    nm.delete()

    nm_l = user_login.objects.filter(utype='staff')
    context ={'staff_list':nm_l}
    return render(request,'medico/182 Medico -DOC/admin_staff_user_view.html',context)

def admin_staff_user_view(request):
    nm_l = user_login.objects.filter(utype='staff')
    context = {'staff_list': nm_l}
    return render(request, 'medico/182 Medico -DOC/admin_staff_user_view.html', context)

def admin_user_delete(request):
    id = request.GET.get('id')
    print("id="+id)

    nm = user_login.objects.get(id=int(id))
    nm.delete()

    nm_l = user_login.objects.filter(utype='patient')
    context ={'user_list':nm_l}
    return render(request,'medico/182 Medico -DOC/admin_user_view.html',context)

def admin_user_view(request):
    nm_l = user_login.objects.filter(utype='patient')
    context = {'user_list': nm_l}
    return render(request, 'medico/182 Medico -DOC/admin_user_view.html', context)

def admin_data_set_master_add(request):
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()
        pic_path = fs.save(uploaded_file.name, uploaded_file)
        category_id = int(request.POST.get('category_id'))
        dt = datetime.today().strftime('%Y-%m-%d')
        tm = datetime.today().strftime('%H:%M:%S')
        status='ok'
        data_obj = data_set_master(pic_path=pic_path,category_id=category_id,dt=dt,tm=tm,status=status)
        data_obj.save()
        category_list = category_master.objects.all()
        context = {'category_list': category_list }
        return render(request, 'medico/182 Medico -DOC/admin_data_set_master_add.html',context)
    else:
        category_list = category_master.objects.all()
        context = {'category_list': category_list}
        return render(request, 'medico/182 Medico -DOC/admin_data_set_master_add.html',context)


def admin_data_set_master_delete(request):
    id = request.GET.get('id')
    print("id="+id)
    lm = data_set_master.objects.get(id=int(id))
    lm.delete()

    pp_l = data_set_master.objects.all()
    cm_l = category_master.objects.all()
    cmd = {}
    for nm in cm_l:
        cmd[nm.id] = nm.category_name
    context = {'pic_list':pp_l,'category_list':cmd}
    return render(request,'medico/182 Medico -DOC/admin_data_set_master_view.html',context)

def admin_data_set_master_view(request):
    pp_l = data_set_master.objects.all()
    cm_l = category_master.objects.all()
    cmd = {}
    for nm in cm_l:
        cmd[nm.id] = nm.category_name
    context = {'pic_list': pp_l, 'category_list': cmd}
    return render(request, 'medico/182 Medico -DOC/admin_data_set_master_view.html', context)


######STAFF###########
def staff_login(request):
    if request.method == 'POST':
        uname = request.POST.get('uname')
        passwd = request.POST.get('passwd')

        ul = user_login.objects.filter(uname=uname, password=passwd,utype='staff')

        if len(ul) == 1:
            request.session['user_id'] = ul[0].id
            request.session['user_name'] = ul[0].uname
            context = {'uname': request.session['user_name']}
            return render(request, 'medico/182 Medico -DOC/staff_home.html',
                          context)
        else:
            return render(request, 'medico/staff_login.html')
    else:
        return render(request, 'medico/staff_login.html')

def staff_home(request):

    context = {'uname':request.session['user_name']}
    return render(request,'./medico/182 Medico -DOC/staff_home.html',context)

def staff_changepassword(request):
    if request.method == 'POST':
        uname = request.session['user_name']
        new_password = request.POST.get('new_password')
        current_password = request.POST.get('current_password')
        print("username:::" + uname)
        print("current_password" + str(current_password))

        try:

            ul = user_login.objects.get(uname=uname, password=current_password)

            if ul is not None:
                ul.password = new_password  # change field
                ul.save()
                return render(request, './medico/182 Medico -DOC/staff_settings.html')
            else:
                return render(request, './medico/182 Medico -DOC/staff_settings.html')
        except user_login.DoesNotExist:
            return render(request, './medico/182 Medico -DOC/staff_changepassword.html')
    else:
        return render(request, './medico/182 Medico -DOC/staff_changepassword.html')

def staff_settings(request):

    context = {'uname':request.session['user_name']}
    return render(request,'./medico/182 Medico -DOC/staff_settings.html',context)

def staff_patient_test_master_add(request):
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()

        pic_path = fs.save(randomString(10)+uploaded_file.name, uploaded_file)
        print(pic_path)

        pic_list = data_set_master.objects.all()
        cnt = 0
        cnt1= 0
        selfile=''
        selcat=0
        for pic in pic_list:
            file1 = f'./myapp/static/myapp/media/{pic.pic_path}'
            file2 = f'./myapp/static/myapp/media/{pic_path}'
            cnt1 = orb_compute(file1,file2,cnt)
            if cnt1 >= cnt:
                selfile=file1
                selcat=pic.category_id
            cnt = cnt1
            print(f"{file1},{file2},{cnt}")

        print(selfile)
        print(selcat)
        fname = request.POST.get('fname')
        lname = request.POST.get('lname')
        dob = request.POST.get('dob')
        gender = request.POST.get('gender')
        pno = request.POST.get('pno')
        addr = request.POST.get('addr')
        pincode = request.POST.get('pincode')
        email = request.POST.get('email')
        contact = request.POST.get('contact')
        password = '1234'
        uname = email
        status = "new"
        dt = datetime.today().strftime('%Y-%m-%d')
        tm = datetime.today().strftime('%H:%M:%S')

        ul = user_login(uname=uname, password=password, utype='patient')

        ul.save()
        user_id = user_login.objects.all().aggregate(Max('id'))['id__max']

        ud = patient_details(user_id=user_id, fname=fname, lname=lname, dob=dob,gender=gender, addr=addr, pincode=pincode, conatct=contact,
                          status=status, email=email,pno=pno,dt=dt,tm=tm)
        ud.save()

        cm = category_master.objects.get(id=int(selcat))

        #staff_user_id = int(request.session['user_id'])
        result = cm.category_name



        utm = patient_report(patient_id=user_id, file_path=pic_path, result=result, dt=dt, tm=tm,
                               status=status)
        utm.save()

        context = {'category_name': cm.category_name}
        return render(request, 'medico/182 Medico -DOC/staff_patient_test_result.html',context)
    else:
        context = {}

        return render(request, 'medico/182 Medico -DOC/staff_patient_test_master_add.html',context)


def staff_patient_test_master_view(request):
    pp_l = patient_report.objects.all()
    ud_l = patient_details.objects.all()
    cmd = {}
    for nm in ud_l:
        cmd[nm.user_id] = f'{nm.fname} {nm.lname}'
    context = {'test_list': pp_l, 'user_list': cmd}
    return render(request, 'medico/182 Medico -DOC/staff_patient_test_master_view.html', context)

def admin_staff_test_delete(request):
    id = request.GET.get('id')
    print("id=" + id)
    lm = patient_report.objects.get(id=int(id))
    lm.delete()


    pp_l = patient_report.objects.all()
    ud_l = patient_details.objects.all()
    cmd = {}
    for nm in ud_l:
        cmd[nm.user_id] = f'{nm.fname} {nm.lname}'
    context = {'test_list': pp_l, 'user_list': cmd}
    return render(request, 'medico/182 Medico -DOC/staff_patient_test_master_view.html', context)




###########################################
########USER#############
def user_login_check(request):
    if request.method == 'POST':
        uname = request.POST.get('uname')
        passwd = request.POST.get('passwd')

        ul = user_login.objects.filter(uname=uname, password=passwd,utype='patient')
        print(len(ul))
        if len(ul) == 1:
            request.session['user_id'] = ul[0].id
            request.session['user_name'] = ul[0].uname
            context = {'uname': request.session['user_name']}
            return render(request, 'medico/182 Medico -DOC/user_home.html',context)
        else:
            context={'msg':'Login Error'}
            return render(request, 'medico/user_login.html',context)
    else:
        return render(request, 'medico/user_login.html')

def user_home(request):

    context = {'uname':request.session['user_name']}
    return render(request,'./medico/182 Medico -DOC/user_home.html',context)

def user_changepassword(request):
    if request.method == 'POST':
        uname = request.session['user_name']
        new_password = request.POST.get('new_password')
        current_password = request.POST.get('current_password')
        print("username:::" + uname)
        print("current_password" + str(current_password))

        try:

            ul = user_login.objects.get(uname=uname, password=current_password)

            if ul is not None:
                ul.password = new_password  # change field
                ul.save()
                context = {'msg':'Password changed'}
                return render(request, './medico/182 Medico -DOC/user_settings.html',context)
            else:
                context = {'msg': 'Password change error'}
                return render(request, './medico/182 Medico -DOC/user_settings.html',context)
        except user_login.DoesNotExist:
            context = {'msg': 'Password change error'}
            return render(request, './medico/182 Medico -DOC/user_changepassword.html',context)
    else:
        return render(request, './medico/182 Medico -DOC/user_changepassword.html')

def user_settings(request):

    context = {'uname':request.session['user_name']}
    return render(request,'./medico/182 Medico -DOC/user_settings.html',context)

def patient_patient_test_master_view(request):
    patient_id = int(request.session['user_id'])
    pp_l = patient_report.objects.filter(patient_id=patient_id)
    ud_l = patient_details.objects.all()
    cmd = {}
    for nm in ud_l:
        cmd[nm.user_id] = f'{nm.fname} {nm.lname}'
    context = {'test_list': pp_l, 'user_list': cmd}
    return render(request, 'medico/182 Medico -DOC/patient_patient_test_master_view.html', context)


########################################################

import random
import string

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

